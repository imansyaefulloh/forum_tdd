<?php

use App\User;
use App\Reply;
use App\Thread;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        // populate users
        factory(User::class)->create([
            'name' => 'Iman Syaefulloh',
            'email' => 'iman@gmail.com',
            'password' => '$2y$10$Lgr58q7PcnSTQ05nzHpzLuNtDdtMKBV/Qp5zeoRYiGCYgThT95gfq' // secret
        ]);

        factory(User::class)->create([
            'name' => 'Amin Rais',
            'email' => 'amin@gmail.com',
            'password' => '$2y$10$Lgr58q7PcnSTQ05nzHpzLuNtDdtMKBV/Qp5zeoRYiGCYgThT95gfq' // secret
        ]);

        // populate threads
        $thread = factory(Thread::class, 3)->create();
        $threads = factory(Thread::class, 50)->create();
        $threads->each(function($thread) {
            factory(Reply::class, 10)->create(
                ['thread_id' => $thread->id]
            );
        });
    }
}
